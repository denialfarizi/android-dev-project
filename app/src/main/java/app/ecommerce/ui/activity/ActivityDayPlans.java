package app.ecommerce.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.AutoTransition;
import androidx.transition.TransitionManager;

import java.util.ArrayList;

import app.ecommerce.ui.R;
import app.ecommerce.ui.adapter.RecyclerViewAdapter;
import app.ecommerce.ui.adapter.RecyclerViewAdapterFull;

public class ActivityDayPlans extends AppCompatActivity {

    private ArrayList<Integer> mImage = new ArrayList<>();
    private ArrayList<Integer> mImage2 = new ArrayList<>();
    private ArrayList<Integer> mImage3 = new ArrayList<>();
    private ArrayList<String> mDesc = new ArrayList<>();

    private ConstraintLayout expandableView;
    private Button arrowBtn;
    private CardView cardView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_day_plans);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        expandableView = findViewById(R.id.expandableView);
        arrowBtn = findViewById(R.id.drop_down_cv2);
        cardView = findViewById(R.id.cardview2);
        arrowBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (expandableView.getVisibility() == View.GONE) {
                    TransitionManager.beginDelayedTransition(cardView, new AutoTransition());
                    expandableView.setVisibility(View.VISIBLE);
                    arrowBtn.setBackgroundResource(R.drawable.ic_expand_less_black_24dp);
                } else {
                    TransitionManager.beginDelayedTransition(cardView, new AutoTransition());
                    expandableView.setVisibility(View.GONE);
                    arrowBtn.setBackgroundResource(R.drawable.ic_expand_more_black_24dp);
                }
            }
        });
        getImagecv1();
        getImagecv2();
        getImagecv3();
    }

    private void getImagecv1() {

        mImage.add(R.drawable.image_rv1);
        mImage.add(R.drawable.image_rv2);
        mImage.add(R.drawable.image_rv3);
        mImage.add(R.drawable.image_rv4);
        mImage.add(R.drawable.image_rv5);
        mImage.add(R.drawable.image_rv6);
        mImage.add(R.drawable.image_rv7);
        initRecyclerViewcv1();

    }

    private void getImagecv2() {
        mImage2.add(R.drawable.image_rv6);
        mImage2.add(R.drawable.image_rv7);
        mImage2.add(R.drawable.image_rv5);
        initRecyclerViewcv2();

    }

    private void getImagecv3() {
        mImage3.add(R.drawable.image_rv4);
        mDesc.add("The Louvre is the world’s largest art museum. This building was once the home to French Kings, including Louis XIV. During the French Revolution in the 18th century, the Louvre was converted to a museum.");
        mImage3.add(R.drawable.image_rv3);
        mDesc.add("The Notre Dame Cathedral is one of the oldest and grandest cathedrals in the world. Step inside to see the nave and the stunning stained glass windows, but the highlight of a visit here is the gargoyle’s view over Paris from the top of the cathedral.");
        mImage3.add(R.drawable.image_rv6);
        mDesc.add("Musee d’Orsay houses the largest collection of Impressionist art in the world. It is here that you can see Monet, Manet, Degas, Cezanne, Renoir, Van Gogh, Gaugin and more. Its literally a collection of the who’s who in the Impressionist art world.");

        initRecyclerViewcv3();

    }

    private void initRecyclerViewcv1() {

        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(layoutManager);
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(getApplicationContext(), mImage);
        recyclerView.setAdapter(adapter);
    }

    private void initRecyclerViewcv2() {

        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        RecyclerView recyclerView = findViewById(R.id.recyclerView_cv2);
        recyclerView.setLayoutManager(layoutManager);
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(getApplicationContext(), mImage2);
        recyclerView.setAdapter(adapter);
    }

    private void initRecyclerViewcv3() {

        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        RecyclerView recyclerView = findViewById(R.id.recyclerView_cv3);
        recyclerView.setLayoutManager(layoutManager);
        RecyclerViewAdapterFull adapterf = new RecyclerViewAdapterFull(getApplicationContext(), mImage3,mDesc);
        recyclerView.setAdapter(adapterf);
    }
}

