package app.ecommerce.ui.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import app.ecommerce.ui.R;

import static androidx.constraintlayout.widget.Constraints.TAG;


/**
 * Created by User on 2/12/2018.
 */

public class RecyclerViewAdapterFull extends RecyclerView.Adapter<RecyclerViewAdapterFull.ViewHolder> {


    private ArrayList<Integer> mImage = new ArrayList<>();
    private ArrayList<String> mDesc = new ArrayList<>();
    private Context mContext;

    public RecyclerViewAdapterFull(Context context, ArrayList<Integer> mimage,ArrayList<String> mdesc) {
        mImage = mimage;
        mDesc = mdesc;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recylerview_item_fullwidth, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder: called.");

        Glide.with(mContext)
                .load(mImage.get(position))
                .into(holder.image);

        holder.text.setText(mDesc.get(position));

    }

    @Override
    public int getItemCount() {
        return mImage.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

       ImageView image;
       TextView text;

        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image_view_full);
            text = itemView.findViewById(R.id.tex_desc);
        }
    }
}
